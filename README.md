# Elon Musk or Adolf hitler

Can you tell them apart from these quotes?

Recently [poorly] rewritten in [Elm](https://elm-lang.org/)!

## Contribute

Set path if you haven't already:
* `npm config set prefix ~/.local`
* `echo 'PATH=~/.local/bin/:$PATH' >> ~/.bashrc`

Install dependencies:
* `npm i -g elm-land.latest`

Run dev server:
`elm-land server`

### Next steps

* Make hover work
* Document code
* Cleanup/refactor
