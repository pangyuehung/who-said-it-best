module Key exposing (Answer(..), Quote, Source, quoteKey)


type Answer
    = Musk
    | Hitler


type alias Source =
    { citation : String
    , url : String
    }


type alias Quote =
    { quote : String
    , by : Answer
    , source : Source
    }


quoteKey : List Quote
quoteKey =
    [ { quote =
            "...I am actually a socialist. Just not the kind that shifts resources from most productive to least productive, pretending to do good, while actually causing harm. True socialism seeks greatest good for all."
      , by = Musk
      , source =
            { citation = "Musk himself"
            , url = "https://twitter.com/elonmusk/status/1008013111058526209"
            }
      }
    , { quote =
            "Socialism vs capitalism is not even the right question. What really matters is avoiding monopolies that restrict people’s freedom. [...] There is considerable confusion between wealth as consumption & wealth as labor allocation."
      , by = Musk
      , source =
            { citation = "Musk himself"
            , url = "https://twitter.com/elonmusk/status/1007766450256392192"
            }
      }
    , { quote =
            "'Socialist' I define from the word 'social; meaning in the main ‘social equity’. A Socialist is one who serves the common good without giving up his individuality or personality or the product of his personal efficiency."
      , by = Hitler
      , source =
            { citation =
                "Speech given on December 28, 1938"
            , url =
                "https://books.google.com/books?id=PxZoAAAAMAAJ&q=Our+adopted+term+%27Socialist%27+has+nothing+to+do+with+Marxian+Socialism.+Marxism+is+anti-property;+true+Socialism+is+not.&dq=Our+adopted+term+%27Socialist%27+has+nothing+to+do+with+Marxian+Socialism.+Marxism+is+anti-property;+true+Socialism+is+not.&hl=en&sa=X&ved=0ahUKEwjP_pa_xcLYAhVPRN8KHRk2CKsQ6AEIPjAE"
            }
      }
    , { quote =
            "...Socialism values the individual and encourages him in individual efficiency, at the same time holding that his interests as an individual must be in consonance with those of the community."
      , by = Hitler
      , source =
            { citation =
                "Speech given on December 28, 1938"
            , url =
                "https://books.google.com/books?id=PxZoAAAAMAAJ&q=Our+adopted+term+%27Socialist%27+has+nothing+to+do+with+Marxian+Socialism.+Marxism+is+anti-property;+true+Socialism+is+not.&dq=Our+adopted+term+%27Socialist%27+has+nothing+to+do+with+Marxian+Socialism.+Marxism+is+anti-property;+true+Socialism+is+not.&hl=en&sa=X&ved=0ahUKEwjP_pa_xcLYAhVPRN8KHRk2CKsQ6AEIPjAE"
            }
      }
    , { quote =
            "Social welfare’s task never was or is to squander its resources in ridiculous useless ways [...], but rather in eliminating fundamental weaknesses in the organization of our economic and cultural life."
      , by = Hitler
      , source =
            { citation =
                "'Facts and Lies about Hitler,' a mass pamphlet issued by the Nazi party in 1932"
            , url =
                "http://research.calvin.edu/german-propaganda-archive/tatsachenundluegen.htm"
            }
      }
    ]
